import styles from "../styles/Home.module.css";
import { motion } from "framer-motion";
import Link from "next/link";

export default function Home() {
  return (
    <div className={styles.container}>
      <div className={styles["shared-by"]}>
        <b>Shared</b> by you
      </div>

      <div className={styles["main-image-container"]}>
        <motion.img
          layoutId={"tokyo-night"}
          className={styles["main-image"]}
          src={"tokyo.jpg"}
        />
      </div>

      <motion.h1 layoutId="title" className={styles["title"]}>
        Lost in Tokyo
      </motion.h1>

      <motion.div layoutId="description" className={styles.description}>
        Duis aute irure dolor in reprehenderit in voluptate velit esse cillum
        dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
        proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
      </motion.div>

      <div className={styles["title-background"]}>
        <Link href="/full-screen">
          <img
            style={{ filter: "invert(1)" }}
            width="32px"
            src="/full-screen-svgrepo-com.svg"
          ></img>
        </Link>
      </div>
    </div>
  );
}
