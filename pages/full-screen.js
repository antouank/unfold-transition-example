import styles from "../styles/Home.module.css";
import { motion } from "framer-motion";
import Link from "next/link";

const spring = {
  type: "spring",
  damping: 10,
  stiffness: 100,
};

export default function FullScreen() {
  return (
    <div className={`${styles.container} ${styles.inverted}`}>
      <Link href="/">
        <span className={styles.exit}>X</span>
      </Link>

      <motion.img
        layoutId={"tokyo-night"}
        className={styles["main-image-full-screen"]}
        src={"tokyo.jpg"}
      />

      <motion.div layoutId="description" className={styles.description}>
        <motion.h1
          layoutId="title"
          className={styles["title-huge"]}
          transition={spring}
        >
          Lost in Tokyo
        </motion.h1>

        <div className={styles["description-full-screen"]}>
          <div>By Jonathan Vik</div>
          <div>
            8{" "}
            <img
              style={{ filter: "invert(1)" }}
              width="20px"
              src="/comments-svgrepo-com.svg"
            ></img>
          </div>
        </div>
      </motion.div>
    </div>
  );
}
